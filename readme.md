XXX - Manual de uso
===================
**Índice de contenidos**

[TOC]

# 1. Introducción
El presente documento detalla la construcción de un modelo de gestión de proyecto y producto, embebidos en una sola linea de desarrollo, tanto en el tiempo como en la acción. 

# 2. El concepto
xxx está inspirado en el *Círuclo Dorado* de Simon Sinek que propone una mayor receptividad por parte de las personas a las ideas desarrolladas inside out (desde dentro hacia fuera), desde el *por qué*, pasando por el *cómo* y llegando al *qué*.

![circulo dorado](https://lh4.googleusercontent.com/YywYYomSy8K7iCq4u8sSjnlV6weyLwdr8zOQK1ErrZtKkC64zkV-aazRR8pU2Z32BNq4y4wqvfFAGcw=w1342-h547)

xxx se conceptualiza de manera similar:

 - El círculo del **por qué** engloba a las **metas organizacionales**, **cultura empresarial** y al **valor agregado**, que en conjunto a lo largo del desarrollo se entenderán simplemente como ***valor***.
 - El **cómo** contiene el **flujo de actividades de trabajo**
 - Finalmente el **qué** albergará las **tareas** y todos los **recursos** que devengan de estas.

# 3. El modelo
El modelo xxx, propone una secuencia de actividades que vinculen la elaboración del plan de negocios y el desarrollo del producto o servicio afín, puesto que ciertas actividades de negocio que requieren de especial atención y dependen de otras actividades de desarrollo para poder optimizar sus resultados y el uso de recursos.

El modelo se divide en dos hemisferios:

 1. **Norte o de Negocios**, este hemisferio contendrá las actividades y tareas relacionadas con el estudio del negocio desde todas sus prespectivas.
 
 2. **Sur o de Desarrollo**, este hemisferio contendrá a las actividades y tareas directamente relacionadas con la elaboración del producto o servicio a desarrollar.

![diagrama](https://lh5.googleusercontent.com/fmqedrpW8tQlw-iDHy9gdvTSQ1cIkHxwMNTUQxKWVfq99H7OHKgISAHIEdsbGAMnDLfIvDLs=w1332-h537)


**Leyenda**

|Acrónimo | Nombre |Descripción|
| :-------: | :---- | :---- |
| AN1| Actividad de Negocio 1 |Idea de negocio|
| AN2| Actividad de Negocio 2 |Estudio de mercado|
| AD1| Actividad de Desarrollo 1 |Desarrollo del prototipo|
| AN3| Actividad de Negocio 3 |Plan de marketing|
| AN4| Actividad de Negocio 4 |Plan financiero|
| AD2| Actividad de Desarrollo 2 |Desarrollo del producto/servicio|
| T| Tarea |-|
| R| Recurso|-|

# 3.1. Desarrollando XXX

XXX plantea una estructura de trabajo organizada basada en la practicidad, reusabilidad y convergencia de sus resultados. Como se puede apreciar en el diagrama del modelo, el concepto de **valor** busca ser visible desde cualquier punto del desarrollo del negocio o del producto, para que nada pierda alineamiento con **las metas organizacionales** contribuyendo con **la cultura empresarial**.

Pero el **valor** no esta predefinido y tampoco es absoluto, como se observará a lo largo del desarrollo, el **valor** ha a cambiar una o más veces para poder ajustarse a los requerimientos del proyecto, del negocio o de la empresa.

Para desarrollar XXX de manera correcta se debe de seguir el siguiente siguiente orden estrictamente pero los puntos pueden ser desarrollados usando metodologías o conceptos diferente pero **siempre** deben de responder a sus **criterios de evaluación** antes de seguir con el flujo correspondiente.


> **Criterio de Evaluación:** Deber de responder al menos a una de las siguientes preguntas:

> - ¿la idea es innovadora?
> - si la idea no es innovadora ¿se planea una entrada temprana al mercado?

## 2. Estudio de Mercado [AN2]

> **Criterio de Evaluación:** Deber de responder a la pregunta ¿Es factible?

Deber de responder a la pregunta ¿Es factible?, como criterio de evaluación para definir en primera instancia la propuesta de valor. Así mismo responde al negocio la pregunta ¿Qué necesita el mercado?

## 3. Desarrollo del prototipo [AD1]

> **Criterio de Evaluación:** Debe de ejecutar el flujo básico funcional.

Debe de entregar una síntesis que pueda ser explorada y evaluada, no necesariamente funcional que sirva de recurso base al AN3.

## 4. Plan de marketing [AN3]

> **Criterio de Evaluación:** Debe de responder a la pregunta ¿se puede vender?

Debe de responder a la pregunta ¿se puede vender?, como criterio de evaluación para re definir una o más veces la propuesta de valor, ajustandola a la realidad del mercado en evaluación. Así mismo responde al negocio la pregunta ¿Cómo se debe de vender?

## 5. Plan financiero [AN4]
> **Criterio de Evaluación:** Debe de responder a la pregunta ¿es rentable?

Debe de responder a la pregunta ¿es rentable?, como criterio de evaluación, para definir parámetros que re definen una o más veces a la propuesta de valor, alineandola económicamente con los objetivos de la empresa. Así mismo responde al negocio la pregunta ¿Cómo será sustentable el negocio en el tiempo?

## 6. Desarrollo producto/servicio [AD2]
> **Criterio de Evaluación:** Debe de ser funcional

Debe de entregar el producto/servicio terminado que corresponda a todas las características definidas en la última versión de la propuesta de valor.

## T - Tarea

## R - Recurso